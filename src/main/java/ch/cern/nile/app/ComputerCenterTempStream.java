package ch.cern.nile.app;

import ch.cern.nile.app.generated.ComputerCenterTempPacket;
import ch.cern.nile.kaitai.streams.LoraDecoderStream;

public final class ComputerCenterTempStream extends LoraDecoderStream<ComputerCenterTempPacket> {

    public ComputerCenterTempStream() {
        super(ComputerCenterTempPacket.class);
    }
}

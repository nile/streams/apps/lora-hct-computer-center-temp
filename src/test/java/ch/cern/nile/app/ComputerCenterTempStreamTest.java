package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

public class ComputerCenterTempStreamTest extends StreamTestBase {

    private static final JsonObject DATA_FRAME = TestUtils.loadRecordAsJson("data/data_frame.json");
    private static final JsonObject UNSUPPORTED_FRAME = TestUtils.loadRecordAsJson("data/unsupported_frame.json");

    @Override
    public AbstractStream createStreamInstance() {
        return new ComputerCenterTempStream();
    }

    @Test
    void givenCorrectDataFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(DATA_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        assertEquals(31.2, outputRecord.value().get("temperature").getAsDouble());
        assertEquals(29.5, outputRecord.value().get("relativeHumidity").getAsDouble());
        assertEquals(12.2, outputRecord.value().get("differentialPressure").getAsDouble());
        assertEquals(11.4, outputRecord.value().get("dewPoint").getAsDouble());
    }

    @Test
    void givenUnsupportedFrame_whenDecoding_thenIgnoresFrameAndProcessesNextCorrectData() {
        pipeRecord(UNSUPPORTED_FRAME);
        pipeRecord(DATA_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();
        assertEquals(31.2, outputRecord.value().get("temperature").getAsDouble());
    }

}

package ch.cern.nile.app.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

class ComputerCenterTempPacketTest {

    private static final JsonElement DATA_FRAME = TestUtils.getDataAsJsonElement("AWcBKQJoagNnAL8EAv/2");
    private static final JsonElement UNSUPPORTED_FRAME = TestUtils.getDataAsJsonElement("Oh4WCwcX");

    @Test
    void givenDataFrame_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        final Map<String, Object> packet = KaitaiPacketDecoder.decode(DATA_FRAME, ComputerCenterTempPacket.class);
        assertEquals(29.7, packet.get("temperature"));
        assertEquals(53.0, packet.get("relativeHumidity"));
        assertEquals(-0.1, packet.get("differentialPressure"));
        assertEquals(19.1, packet.get("dewPoint"));
    }

    @Test
    void givenUnsupportedFrame_whenDecoding_thenThrowsDecodingException() {
        assertThrows(DecodingException.class,
                () -> KaitaiPacketDecoder.decode(UNSUPPORTED_FRAME, ComputerCenterTempPacket.class));
    }

}
